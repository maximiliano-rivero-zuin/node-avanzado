//Intro a callbacks

//Ejercicio practico
const countries = []

const initContries = () => {
    newCountry('Argentina', showCountries)
    newCountry('Brasil', showCountries)
    newCountry('Bélgica', showCountries)
    newCountry('Sudáfrica', showCountries)
    newCountry('Bosnia y Herzegovina', showCountries)
}
const newCountry = (country, callback) => {
    setTimeout(()=>{
        countries.push(country);
        callback(); 
    },1500)    
}

const showCountries = () => {
        countries.forEach(country => console.log(country))
        console.log('\n')   
}

//Sin CALLBACK
const lenguajes = ['Node', 'React', 'Java', 'Python', 'Ruby on Rails']
function mostrarLenguajes() {
    setTimeout(() => {

        lenguajes.forEach(lenguaje => console.log(lenguaje))

    }, 1000);
}
// mostrarLenguajes() 

//Con CALLBACK
function nuevoLenguaje(lenguaje, callback) {

    setTimeout(() => {

        lenguajes.push(lenguaje);

        callback();

    }, 2000);

}

// nuevoLenguaje('Angular', mostrarLenguajes)

// Callback hell (MALA IMPLEMENTACION): Llamado a múltiples callbacks entre sí mismos.
const lenguajesAprendidos = []

function agregarLenguajeAprendido(lenguaje, callback) {

    lenguajesAprendidos.push(lenguaje);

    console.log(`Agregado: ${lenguaje}`);

    callback();

}

function mostrarLenguajesAprendidos() {

    console.log(lenguajesAprendidos)

}

function iniciarCallbackHell() {

    setTimeout(() => {

        agregarLenguajeAprendido('Java', mostrarLenguajesAprendidos)

        setTimeout(() => {

            agregarLenguajeAprendido('Node', mostrarLenguajesAprendidos)

            setTimeout(() => {

            agregarLenguajeAprendido('Python', mostrarLenguajesAprendidos)

            }, 3000);

        }, 3000);

    }, 3000);

} 

// iniciarCallbackHell();

module.exports = {
    initContries: initContries,
	newCountry: newCountry,
    showCountries: showCountries,
    mostrarLenguajes: mostrarLenguajes,
    nuevoLenguaje : nuevoLenguaje,
    iniciarCallbackHell : iniciarCallbackHell
};