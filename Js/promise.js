const contratarJugador = new Promise( (resolve, reject) => {
    const contratado = true;

    if(contratado) {
        resolve('El jugador se incorpora al equipo')
    } else {
        reject('El jugador y el equipo no llegaron a un acuerdo')
    }
})


const showMessage = (response) =>{
    console.log(`Mensaje: ${response}`)
}

module.exports = {
    contratarJugador: contratarJugador,
    showMessage: showMessage
}